package aspects;
 
import org.aspectj.lang.Signature;

//import util.CallLogger;


public aspect Trace{
	CallLogger callLogger;
	//(call(* *(..))&& !cflow(within(Trace)));//
    pointcut traceMethods() : (execution(* org.apache.lucene..*(..))&& !cflow(within(Trace)));//(execution(* org.apache.lucene..*(..)) && !cflow(within(Trace)));
    
    
	public Trace() {
		callLogger = CallLogger.getInstance();
	}


	before(): traceMethods(){
        Signature sig = thisJoinPointStaticPart.getSignature();
        callLogger.pushMethod(sig);
//        //String line =""+ thisJoinPointStaticPart.getSourceLocation().getLine();
//        String sourceName = thisJoinPointStaticPart.getSourceLocation().getWithinType().getCanonicalName();
//        StringBuilder logMessage = new StringBuilder();
//        logMessage.append("CALL FROM:").append(sourceName).append(" line ").append(thisJoinPointStaticPart.getSourceLocation().getLine()).append(" to ").append(sig.getDeclaringTypeName()).append(".").append(sig.getName());
//        System.out.println(logMessage);
    }
	
	after(): traceMethods(){
		callLogger.popMethod();
	}
    
}

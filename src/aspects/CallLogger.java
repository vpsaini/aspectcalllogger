package aspects;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Stack;

import org.aspectj.lang.Signature;

public class CallLogger {
	public static CallLogger instance;
	Stack callStack;
	String methodSep = "@@::@@";
	char[] hirarchy;
	int hiracchyCounter;
	BufferedWriter writer;
	
	public static CallLogger getInstance(){
		if(instance==null){
			instance = new CallLogger();
		}
		return instance;
	}
	
	private CallLogger() {
		callStack = new Stack();
		hirarchy = new char[(int)(Integer.MAX_VALUE*.9)];
		this.hiracchyCounter=0;
		try {
			///home/sourcerer/hitesh-vaibhav/architechture_recovery/svnrepo/lucene/build/core/test/J0/dummy/
	        writer = new BufferedWriter(new FileWriter("/home/sourcerer/hitesh-vaibhav/architechture_recovery/parallelization/output/calls.txt",true));
	    } catch (IOException e) {
	    	System.out.println("Cannot open 'calls.txt' for writing");
	        throw new RuntimeException("Cannot open 'calls.txt' for writing.", e);
	    }
	}

	public void pushMethod(Signature sig){
    	StringBuffer sb =  new StringBuffer();
    	String fqmName = sb.append(sig.getDeclaringTypeName()).append(".").append(sig.getName()).toString();
    	System.out.println("fqmName: "+ fqmName);
    	this.callStack.push(fqmName);
    	System.out.println("size: "+ this.callStack.size());
    	if(this.callStack.size()<2){
    		this.hiracchyCounter=0;
    		
    	}
    	this.appendToHirarchy(fqmName);
    	this.appendToHirarchy(methodSep);
    }
    private void appendToHirarchy(String input){
    	for(int i=0;i<input.length();i++){
    		char c = input.charAt(i);
    		this.hirarchy[this.hiracchyCounter]=c;
    		this.hiracchyCounter++;
    	}
    }
	
    public void writeToFile(String data){
    	System.out.println("i am here with:\n "+ data);
    	try {
			this.writer.write(data+"\n");
			this.writer.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    
    public void popMethod(){
    	this.callStack.pop();
    	if(this.callStack.size()==0){
    		String data = new String(this.hirarchy,0,this.hiracchyCounter);
    		this.writeToFile(data);
    	}
    }
}
